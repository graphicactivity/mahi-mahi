L.mapbox.accessToken = 'pk.eyJ1IjoiYXNmZXJlbnMiLCJhIjoiUFVpUWtBSSJ9.BkWDrqIrFstKBHAKv9TsvQ';
var map = L.mapbox.map("map")
    .setView([2.4578795, 96.2213211], 15);

var myLayer = L.mapbox.featureLayer().addTo(map);

var styleLayer = L.mapbox.styleLayer('mapbox://styles/asferens/cixa6lbwr00bo2psfvb0ixmhn')
    .addTo(map);

// Add features to the map.
map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
myLayer.setGeoJSON(geoJson);
