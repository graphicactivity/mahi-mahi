jQuery(document).ready(function($) {
    $(".royalSlider").royalSlider({
    	autoScaleSlider: true,
    	globalCaption: true,
    	autoHeight: false,
    	arrowsNav: false,
        controlNavigation: 'bullets',
        imageScaleMode: 'none',
        imageAlignCenter: false,
        keyboardNavEnabled: true,
        slidesSpacing: 0,
        usePreloader: false,
        transitionSpeed: 800,
        transitionType: 'fade',
        sliderDrag: true,
        loop: true,
        block: {
            fadeEffect: true,
            moveEffect: 'top'
        },
        autoPlay: {
            enabled: true,
            pauseOnHover: true,
            delay: 3000
        },
    });

    $(".imageSlider").royalSlider({
            autoScaleSlider: false,
            globalCaption: true,
            autoHeight: true,
            arrowsNav: false,
            controlNavigation: 'bullets',
            imageScaleMode: 'none',
            imageAlignCenter: false,
            keyboardNavEnabled: true,
            slidesSpacing: 120,
            usePreloader: false,
            transitionSpeed: 800,
            transitionType: 'move',
            sliderDrag: true,
            loop: true,
            block: {
                fadeEffect: true,
                moveEffect: 'top'
            },
            autoPlay: {
                enabled: true,
                stopAtAction: false,
                pauseOnHover: true,
                delay: 4000
            },
        });

});

$(window).scroll(function() {
    if ($(window).scrollTop() > 400 ){
        $('#book-now').addClass('show');
    } else {
        $('#book-now').removeClass('show');
    };
});

jQuery(document).ready(function($){
    $(document).ready(function() {
        $(window).scroll( function(){
            $('.reveal').each( function(i){
                var bottom_of_object = $(this).position().top + $(this).outerHeight() / 2;
                var bottom_of_window = $(window).scrollTop() + $(window).height();
                if( bottom_of_window > bottom_of_object ){
                    $(this).animate({'opacity':'1'},1000);  
                }
            }); 
        });
    });
});